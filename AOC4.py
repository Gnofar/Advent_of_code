import itertools

def cleanLine(line):
    line = line.replace("  ", " ")
    if len(line.strip()) != 0 :   
        if line[0] == " ": 
            line = line[1:]
    return line

def firstP (inp):
    bingoNumbers = inp.split("\n")[0]
    bingoTiles = dict()
    inp = inp.split("\n")[2:]
    counter = 0
    bingoTileNumber = 1
    curVerLines = dict()
    for line in inp:
        line = cleanLine(line)
        if counter == 5:
            for key in curVerLines.keys():
                bingoTiles[bingoTileNumber].append(curVerLines[key])
            curVerLines = dict()
            counter = 0
            bingoTileNumber += 1
            continue
        #horisontella linje
        curHorLine = list()
        for el in line.split(" "):
            curHorLine.append(el)
        if bingoTileNumber not in bingoTiles.keys():
            bingoTiles[bingoTileNumber] = list()
        bingoTiles[bingoTileNumber].append(curHorLine)
        #Vertikala linjer
        for i in range(0,5):
            if i in curVerLines.keys():
                curVerLines[i].append(line.split(" ")[i])
            else:
                curVerLines[i] = list()
                curVerLines[i].append(line.split(" ")[i])
        counter = counter + 1
    #Börja bingoa!
    currentBingoNumbers = list()
    for bingoNumber in bingoNumbers.split(","):
        currentBingoNumbers.append(bingoNumber)
        for bingoTile in bingoTiles:
            for bingoRow in bingoTiles[bingoTile]:
                if set(bingoRow) <= set(currentBingoNumbers):
                    unsortedSum = sum(bingoTiles[bingoTile], [])
                    sortedSum = set(unsortedSum) - set(currentBingoNumbers)                      
                    return (sum(map(int ,sortedSum)) * int(currentBingoNumbers[-1]))

f = open("input_4.txt", "r").read()
print(firstP(f))