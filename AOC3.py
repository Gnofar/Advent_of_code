def firstP (inp):
    vals = dict()
    for i in range(0,12):
        vals[i] = int() 
    for i in inp:
        pos = 0
        for y in i:
            if int(y) == 1:  vals[pos] = vals[pos] + 1
            if int(y) == 0:  vals[pos] = vals[pos] -1
            pos = pos + 1
    gr = str()
    for i in range(0,12):
        print(vals[i])
        if vals[i] < 0: gr = gr + "0"
        if vals[i] > 0: gr = gr + "1"
    print(gr)
    return list(gr)
def secondP (inp):
    pos = 0
    currentBin = 0
    mostCommon = int()
    for i in range(0,8):
        for val in inp:
            if int(list(val)[pos]) == 1:
                currentBin = currentBin + 1
            elif int(list(val)[pos]) == 0: currentBin = currentBin - 1
        if currentBin < 0: mostCommon = 0
        if currentBin > 0: mostCommon = 1
        if currentBin == 0: mostCommon = 1
        newInp = list()
        print(currentBin)
        for val in inp:
            if int(list(val)[i]) != mostCommon: newInp.append(val)
        inp = newInp
        pos = pos + 1
    print(inp)
        

f = open("input_3.txt", "r").read().split("\n")
#br = firstP(f)
secondP(f)