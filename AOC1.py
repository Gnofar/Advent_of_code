from os import read


f = open("input_1.txt", "r").read().split("\n")
print(f)
increases = int()
pos = 0
prevTot = f[pos] + f[pos + 2] + f[pos + 3]
for i in f[1:]:
    a = f[pos]
    b = f[pos+1]
    try:
        c = f[pos+2]
    except:
        break
    if f[pos] + f[pos+1] + f[pos+2] > prevTot:
        increases = increases +  1
    prevTot = a+b+c
    pos = pos+1
print(increases)

numbers = [int(x) for x in open('input_1.txt', 'r').readlines()]


windows = [sum(w) for w in zip(numbers, numbers[1:], numbers[2:])]
pairs = zip(windows, windows[1:])
increases = [b - a for a,b in pairs if b - a > 0]
print('part 2:', len(increases))