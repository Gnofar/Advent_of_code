def firstP(input):
    depth = 0
    hor = 0
    for i in f:
        print(i)
        if i.split(" ")[0] == "forward":
            hor = hor + int(i.split(" ")[1])
        if i.split(" ")[0] == "down":
            depth = depth + int(i.split(" ")[1])
        if i.split(" ")[0] == "up":
            depth = depth - int(i.split(" ")[1])
    print(depth * hor)

def secondP(input):
    depth = 0
    hor = 0
    aim = 0
    for i in f:
        if i.split(" ")[0] == "forward":
            hor = hor + int(i.split(" ")[1])
            depth = depth + (aim * int(i.split(" ")[1]))
        if i.split(" ")[0] == "down":
            aim = aim + int(i.split(" ")[1])
        if i.split(" ")[0] == "up":
            aim = aim - int(i.split(" ")[1])
    print(hor * depth)
f = open("input_2.txt", "r").read().split("\n")
secondP(f)